#!/usr/bin/env python

import sys
import getopt
import os
import requests
import json
import time


def main(argv):

    digitalocean_api_url = os.getenv('DIGITALOCEAN_API_URL', 'example.com')
    digitalocean_access_token = os.getenv(
        'DIGITALOCEAN_ACCESS_TOKEN', 'a.c.c.e.s.s')

    rancher_server_ip = None
    rancher_base_url = 'http://{ip}:8080{path}'
    docker_registry_address = os.getenv(
        'DOCKER_REGISTRY_ADDRESS', 'registry.example.com')
    docker_registry_email = os.getenv(
        'DOCKER_REGISTRY_EMAIL', 'email@example.com')
    docker_registry_user = os.getenv('DOCKER_REGISTRY_USER', 'user')
    docker_registry_password = os.getenv(
        'DOCKER_REGISTRY_PASSWORD', 'password')

    print('Buckle up, bonehead!')
    print(' ')
    print('''                 uuuuuuu
             uu$$$$$$$$$$$uu
          uu$$$$$$$$$$$$$$$$$uu
         u$$$$$$$$$$$$$$$$$$$$$u
        u$$$$$$$$$$$$$$$$$$$$$$$u
       u$$$$$$$$$$$$$$$$$$$$$$$$$u
       u$$$$$$$$$$$$$$$$$$$$$$$$$u
       u$$$$$$"   "$$$"   "$$$$$$u
       "$$$$"      u$u       $$$$"
        $$$u       u$u       u$$$
        $$$u      u$$$u      u$$$
         "$$$$uu$$$   $$$uu$$$$"
          "$$$$$$$"   "$$$$$$$"
            u$$$$$$$u$$$$$$$u
             u$"$"$"$"$"$"$u
  uuu        $$u$ $ $ $ $u$$       uuu
 u$$$$        $$$$$u$u$u$$$       u$$$$
  $$$$$uu      "$$$$$$$$$"     uu$$$$$$
u$$$$$$$$$$$uu    """""    uuuu$$$$$$$$$$
$$$$"""$$$$$$$$$$uuu   uu$$$$$$$$$"""$$$"
 """      ""$$$$$$$$$$$uu ""$"""
           uuuu ""$$$$$$$$$$uuu
  u$$$uuu$$$$$$$$$uu ""$$$$$$$$$$$uuu$$$
  $$$$$$$$$$""""           ""$$$$$$$$$$$"
   "$$$$$"                      ""$$$$""
     $$$"                         $$$$"''')
    print(' ')

    # Rancher Server

    print('Creating Rancher Server Host instance...')

    url = digitalocean_api_url + '/v2/droplets'

    headers = {
        'Authorization': 'Bearer ' + digitalocean_access_token,
        'Content-Type': 'application/json',
    }

    user_data = '''#!/bin/bash
					sudo apt-get -y update
					curl -sSL https://get.docker.com/ | sh
					sudo docker run -d --restart=unless-stopped -p 8080:8080 -e CATTLE_API_HOST=http://$(/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'):8080 rancher/server'''

    payload = {
        'name': 'node-01',
        'region': 'nyc3',
        'size': '1gb',
        'image': 'ubuntu-16-04-x64',
        'ssh_keys': ['11332997'],
        'backups': False,
        'ipv6': False,
        'user_data': user_data,
        'private_networking': None,
        'volumes': None,
        'tags': [
            'genesis'
        ]
    }

    r = requests.post(url, headers=headers, data=json.dumps(payload))

    print('Waiting for Rancher Server Host to come online...')

    headers = {
        'Authorization': 'Bearer ' + digitalocean_access_token,
    }

    while True:
        try:
            r = requests.get(url, headers=headers)
            json_response = r.json()
            rancher_server_ip = json_response['droplets'][0]['networks']['v4'][0]['ip_address']
            print('Rancher Server Host online!')
            break
        except IndexError:
            print('.')
            time.sleep(10)

    print('Rancher Server Host IP: {ip}'.format(ip=rancher_server_ip))

    rancher_url = rancher_base_url.format(ip=rancher_server_ip, path='/')

    print('Rancher UI will be available @ ' + rancher_url)

    print('Waiting for Rancher UI to be available...')

    # Projects

    # get project

    rancher_project_url = rancher_base_url.format(
        ip=rancher_server_ip, path='/v1/projects/')

    while True:
        try:
            r = requests.get(rancher_project_url)
            json_response = r.json()
            rancher_project_id = json_response['data'][0]['id']
            break
        except:
            print('.')
            time.sleep(10)

    print('Rancher Server online!')

    print('Rancher Project ID: ' + rancher_project_id)

    rancher_project_path = rancher_project_url + rancher_project_id

    # Registries

    # create registry

    print('Creating Registry...')

    rancher_registries_url = rancher_project_path + '/registries'

    headers = {
        'Content-Type': 'application/json',
    }

    payload = {
        "description": "registry.gitlab.com",
        "name": "registry.gitlab.com",
        "serverAddress": "registry.gitlab.com"
    }

    r = requests.post(rancher_registries_url, headers=headers,
                      data=json.dumps(payload))
    json_response = r.json()
    docker_registry_id = json_response['id']

    print('Registry Created!')

    # create registry credential

    print('Creating Registry Credentials...')

    rancher_registries_credential_url = rancher_project_path + '/registryCredentials'

    headers = {
        'Content-Type': 'application/json',
    }

    payload = {
        "description": "registry.gitlab.com credentials",
        "email": docker_registry_email,
        "name": docker_registry_user,
        "publicValue": docker_registry_user,
        "registryId": docker_registry_id,
        "secretValue": docker_registry_password
    }

    r = requests.post(rancher_registries_credential_url,
                      headers=headers, data=json.dumps(payload))

    print('Registry Credentials Created!')

    # Rancher Host Registration

    rancher_server_registration_url = rancher_project_path + '/registrationtokens'

    # create registration token

    print('Creating Host Registration Token...')

    r = requests.post(rancher_server_registration_url)

    # get registration token

    while True:
        try:
            r = requests.get(rancher_server_registration_url)
            json_response = r.json()
            rancher_reg_command = json_response['data'][0]['command']
            if rancher_reg_command == None:
                raise ValueError('The day is too frabjous.')
            else:
                break
        except:
            print('.')
            time.sleep(10)

    print('Host Registration Token Created!')

    # create Rancher host instances

    print('Creating Rancher Host Instances...')

    url = digitalocean_api_url + '/v2/droplets'

    headers = {
        'Authorization': 'Bearer ' + digitalocean_access_token,
        'Content-Type': 'application/json',
    }

    user_data = '''#!/bin/bash
					sudo apt-get -y update
					curl -sSL https://get.docker.com/ | sh
					{command}'''.format(command=rancher_reg_command)

    payload = {
        'names': ['node-02', 'node-03', 'node-04'],
        'region': 'nyc3',
        'size': '1gb',
        'image': 'ubuntu-16-04-x64',
        'ssh_keys': ['11332997'],
        'backups': False,
        'ipv6': False,
        'user_data': user_data,
        'private_networking': None,
        'volumes': None,
        "tags": [
            "genesis"
        ]
    }

    r = requests.post(url, headers=headers, data=json.dumps(payload))

    print('Rancher Host Instances Created!')

    print('Your Rancher Hosts will be available in a few minutes!')

    # Environments (Stacks)

    # create environment

    print('Creating Rancher Environment...')

    rancher_environments_url = rancher_project_path + '/environments'

    headers = {
        'Content-Type': 'application/json',
    }

    payload = {
        "description": "Webflux",
        "name": "Webflux"
    }

    r = requests.post(rancher_environments_url,
                      headers=headers, data=json.dumps(payload))
    json_response = r.json()
    rancher_environment_id = json_response['id']

    print('Rancher Environment Created!')

    print('Rancher Environment ID: ' + rancher_environment_id)

    # Services

    rancher_services_url = rancher_project_path + '/services'

    headers = {
    	'Content-Type': 'application/json',
    }

    # create rabbitmq service

    # If desired, you may also provision default services.

    # Example service creation:
    '''
    print('Creating Rabbit Service...')

    payload = {
      "description": "rabbitmq:3-management",
      "environmentId": rancher_environment_id,
      "launchConfig": {
        "imageUuid": "docker:rabbitmq:3-management",
        "startOnCreate": True
      },
      "name": "rabbit",
      "scale": 1,
      "startOnCreate": True
    }

    r = requests.post(rancher_services_url, headers=headers, data=json.dumps(payload))
    json_response = r.json()
    rancher_rabbit_service_id = json_response['id']

    print('Rabbit Service Created!')

    print('Rabbit Service ID: ' + rancher_rabbit_service_id)
    '''

    print("fin.")


if __name__ == "__main__":
    main(sys.argv[1:])
