# Genesis
Provisions hosts and deploys a Rancher environment on Digital Ocean

The provisioning script will create 4 nodes:
- node-01
    - Rancher Server node
    - Ubuntu-16-04-x64, 1GB
- node-02, node-03, node-04
    - Rancher Host nodes
    - Ubuntu-16-04-x64, 1GB

@TODO: make node provisioning configurable

## Configuration
Set the following values in the .env file:

- DIGITALOCEAN_ACCESS_TOKEN
    - Your Digital Ocean API Access Token
- DOCKER_REGISTRY_ADDRESS
    - The address of the private Docker registry you wish to add to Rancher registries
- DOCKER_REGISTRY_EMAIL
    - The Docker registry email address
- DOCKER_REGISTRY_USER
    - The Docker registry user
- DOCKER_REGISTRY_PASSWORD
    - The Docker registry password

## Build
`$ docker build -t genesis .`

## Run
`$ docker-compose up`
